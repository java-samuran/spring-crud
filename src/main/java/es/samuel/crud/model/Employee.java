package es.samuel.crud.model;

import jakarta.persistence.*;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Entity
@Table(name="employees")
@ToString
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_id", unique = true)
    @Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Formato de correo no valido")
    private String emailId;

    public Employee() {

    }

    public Employee(String firstName, String lastName, String emailId){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.emailId = emailId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}

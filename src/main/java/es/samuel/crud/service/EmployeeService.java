package es.samuel.crud.service;

import es.samuel.crud.model.Employee;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface EmployeeService {
    List<Employee> getAllEmployees();
    Employee createEmployee(Employee employee);

    ResponseEntity<Employee> getEmployeeById(Long id);

    ResponseEntity<Employee> updateEmployee(Long id, Employee employeeDetails);
    ResponseEntity<Map<String, Boolean>> deleteEmployee(Long id);
}

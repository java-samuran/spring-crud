package es.samuel.crud.service.impl;

import es.samuel.crud.exception.ResourceNotFoundException;
import es.samuel.crud.model.Employee;
import es.samuel.crud.repository.EmployeeRepository;
import es.samuel.crud.service.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    private final Logger log = LogManager.getLogger(EmployeeServiceImpl.class);

    @Override
    public List<Employee> getAllEmployees() {
        log.info("Listado empleados");
        return employeeRepository.findAll();
    }

    @Override
    public Employee createEmployee(Employee employee) {
        log.info("Empleado creado: {}", employee);
        return employeeRepository.save(employee);
    }

    @Override
    public ResponseEntity<Employee> getEmployeeById(Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("El empleado con ID: " + id + "no existe"));
        return ResponseEntity.ok(employee);
    }

    @Override
    public ResponseEntity<Employee> updateEmployee(Long id, Employee employeeDetails) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("El empleado con ID: " + id + "no existe"));
        employee.setFirstName(employeeDetails.getFirstName());
        employee.setLastName(employeeDetails.getLastName());
        employee.setEmailId(employeeDetails.getEmailId());

        Employee updatedEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok(updatedEmployee);
    }

    @Override
    public ResponseEntity<Map<String, Boolean>> deleteEmployee(Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("El empleado con ID: " + id + "no existe"));
        employeeRepository.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Borrado", Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}

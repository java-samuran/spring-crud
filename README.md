# TO DO

- Manejo de excepciones (Cuando llamas a /, que sólo te aparezca un 404, no todo el chorrazo)
- Añadir logs a peticiones
- Comprobar campo Email, que valide y no permita duplicados
- Dockerizar con Jib
- Benchmark con limite de memoria
- Cambiar Setter y Getter del model (?)
- Añadir roles, sólo lectura, creación-eliminación de empleados
- Separar cada acción en un microservicio (?)
- Servicio web integrado con LDAP/Keycloak
- 3 microservicios que se comuniquen entre ellos por Consul
- Microservicios con actuator
- Microservicios que vayan por DNS y HTTPS
- Añadir Swagger

Proyectos de referencia/tomar ideas: 
- <https://www.javaguides.net/2021/08/spring-boot-postgresql-crud-example.html> (este fue el principal)
- <https://www.bezkoder.com/spring-boot-postgresql-example/>
- <https://github.com/Angel-Raa/CRUD-spring-boot-postgresql/tree/main>